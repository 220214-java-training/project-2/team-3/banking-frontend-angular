import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { checkingaccountinfo } from './checkingaccount';

@Injectable({
  providedIn: 'root'
})
export class CheckingaccountinfoService {

  constructor(private http: HttpClient,private CookieFactory:CookieService) { }

  ID = this.CookieFactory.get("ID");
  Loggedin = this.CookieFactory.get("loggedIn");
  private id: string = this.ID.toString();
  private aUrl = 'http://localhost:8888/checking/findall/'+this.id;
  private bUrl = 'http://localhost:8888/checking/create';
  private cUrl = 'http://localhost:8888/checking/delete';
  private dUrl = 'http://localhost:8888/checking/update';
  private eUrl = 'http://localhost:8888/checking/findall/0';

  public getbyid(url:string){
    if (this.Loggedin == "true")
    {
      return this.http.get<checkingaccountinfo>(url);
    }

    return this.http.get<checkingaccountinfo>(url);


  }


  public getCheckingaccountinfo(): Observable<checkingaccountinfo[]> {
    if (this.Loggedin == "true")
    {
      return this.http.get<checkingaccountinfo[]>(this.aUrl);
    }

    return this.http.get<checkingaccountinfo[]>(this.eUrl);


  }


  public createCheckingaccount(checkingaccountinfo:checkingaccountinfo): Observable<checkingaccountinfo> {
    return this.http.post<checkingaccountinfo>(this.bUrl,checkingaccountinfo);
  }

  public updateCheckingaccount(checkingaccountinfo:checkingaccountinfo): Observable<checkingaccountinfo> {
    return this.http.put<checkingaccountinfo>(this.dUrl,checkingaccountinfo);
  }

  public deleteCheckingaccount(dummy_pk: number): Observable<void> {
    return this.http.delete<void>(this.dUrl);
  }
}
