import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Passwords } from '../passwords';
import { Profile } from '../register/profileModel';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {

  constructor(private http:HttpClient, private CookieFactory:CookieService) { }

    email:string;
    verifyDestination = 'http://localhost:8888/verifyEmail';
    updateDestination = 'http://localhost:8888/updatePassword';
    verified =  this.CookieFactory.get('verified');
    ID = this.CookieFactory.get('ID');
    
      twoPasswords = new Passwords();

      currentDetails = new Profile();

  ngOnInit(): void {

    
  }
  

  onVerify(){
    let parameter = new HttpParams().set("email",this.email);

    this.http.post(this.verifyDestination,parameter, {withCredentials:true}).subscribe(
      success  =>  {window.location.reload();
        
        
      },

      fail =>   window.location.reload()

    );
}

  onUpdate(){
    let parameters = new HttpParams().set("pass",this.twoPasswords.pass)
                                    .set("passConfirm",this.twoPasswords.passConfirm)
                                    .set("ID",this.ID);

    this.http.put(this.updateDestination,parameters, {withCredentials:true}).subscribe(
      success  =>  window.location.reload(),
                                
                                
      fail =>   window.location.reload()
                                
      );


  }

  

}
