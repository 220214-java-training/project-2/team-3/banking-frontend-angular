import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { savingsaccountinfo } from './savingsaccount';
@Injectable({
  providedIn: 'root'
})
export class SavingsaccountinfoService {

  constructor(private http: HttpClient,private CookieFactory:CookieService) { }

  ID = this.CookieFactory.get("ID");
  Loggedin = this.CookieFactory.get("loggedIn");
  private id: string = this.ID.toString();
  private aUrl = 'http://localhost:8888/savings/findall/'+this.id;
  private bUrl = 'http://localhost:8888/savings/create';
  private cUrl = 'http://localhost:8888/savings/delete';
  private dUrl = 'http://localhost:8888/savings/update';
  private eUrl = 'http://localhost:8888/savings/findall/0';

  public getbyid(url:string): Observable<savingsaccountinfo> {
    if (this.Loggedin == "true")
    {
      return this.http.get<savingsaccountinfo>(url);
    }

    return this.http.get<savingsaccountinfo>(url);


  }


  public getsavingsaccountinfo(): Observable<savingsaccountinfo[]> {
    if (this.Loggedin == "true")
    {
      return this.http.get<savingsaccountinfo[]>(this.aUrl);
    }

    return this.http.get<savingsaccountinfo[]>(this.eUrl);;


  }


  public createsavingsaccount(savingsaccountinfo:savingsaccountinfo): Observable<savingsaccountinfo> {
    return this.http.post<savingsaccountinfo>(this.bUrl,savingsaccountinfo);
  }

  public updatesavingsaccount(savingsaccountinfo:savingsaccountinfo): Observable<savingsaccountinfo> {
    return this.http.put<savingsaccountinfo>(this.dUrl,savingsaccountinfo);
  }

  public deletesavingsaccount(dummy_pk: number): Observable<void> {
    return this.http.delete<void>(this.dUrl);
  }
}
