import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Profile } from '../register/profileModel';


@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {

  constructor(private CookieFactory:CookieService, private http:HttpClient) { }

 
   username = this.CookieFactory.get("username");
   firstName = this.CookieFactory.get("firstName");
   lastName = this.CookieFactory.get("lastName");
   loggedIn = this.CookieFactory.get("loggedIn");
   ID = this.CookieFactory.get("ID");
   destination = 'http://localhost:8888/myProfilePage';
   updatePath = 'http://localhost:8888/updateAccount';


   currentDetails = new Profile();


   ngOnInit(): void {

    if(this.loggedIn=='true')
    {
        let parameters = new HttpParams().set('ID',this.ID);

        this.http.post<any>(this.destination,parameters).subscribe(
          responseBody => {
              this.currentDetails.fname = responseBody.fname;
              this.currentDetails.lname = responseBody.lname;
              this.currentDetails.midInitial = responseBody.middleInit;
              this.currentDetails.street = responseBody.street;
              this.currentDetails.city = responseBody.city;
              this.currentDetails.state = responseBody.state;
              this.currentDetails.zipcode = responseBody.zip;
              this.currentDetails.ssNum = responseBody.ssNum;
              this.currentDetails.uname = responseBody.uname;
              this.currentDetails.email = responseBody.email;
              this.currentDetails.phone = responseBody.phone;
              this.currentDetails.clientID = responseBody.clientID;
          
          },
          responseError => {console.error()}
          
        );
        console.log(this.currentDetails);  
    }

    else 
    {
        

    }
    
    
  

  }

  onUpdate() {let parameters = new HttpParams()
    .set('fname',this.currentDetails.fname)
    .set('midInitial',this.currentDetails.midInitial)
    .set('lname',this.currentDetails.lname)
    .set('email',this.currentDetails.email)
    .set('phone',this.currentDetails.phone)
    .set('street',this.currentDetails.street)
    .set('city',this.currentDetails.city)
    .set('state',this.currentDetails.state)
    .set('zipcode',this.currentDetails.zipcode)
    .set('ssNum',this.currentDetails.ssNum)
    .set('uname',this.currentDetails.uname)
    .set('pass',this.currentDetails.pass)
    .set('passConfirm',this.currentDetails.passConfirm)
    .set('ID',this.CookieFactory.get('ID'));

    this.http.put(this.updatePath,parameters).subscribe(
      success => {window.location.reload(); 
        console.log("put executed successfully");},
      fail => {console.error()}
      

    );

   


  }

}
