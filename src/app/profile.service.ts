import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Profile } from './register/profileModel';



@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http:HttpClient) {}

  destination = 'http://localhost:8888/registerNewAccount';


  createNewProfile(profile:Profile){

    return this.http.post<any>(this.destination,profile);


  }

}
