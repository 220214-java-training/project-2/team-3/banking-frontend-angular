import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { checkingaccountinfo } from '../checkingaccount';
import { CheckingaccountinfoService } from '../checkingaccountinfo.service';
import { savingsaccountinfo } from '../savingsaccount';
import { SavingsaccountinfoService } from '../savingsaccountinfo.service';

@Component({
  selector: 'app-myaccounts',
  templateUrl: './myaccounts.component.html',
  styleUrls: ['./myaccounts.component.css']
})
export class MyaccountsComponent implements OnInit {


  public checkingaccountinfo: checkingaccountinfo[];
  public savingsaccountinfo: savingsaccountinfo[];

  constructor(private checkingaccount: CheckingaccountinfoService, private CookieFactory:CookieService,
    private savingsaccount: SavingsaccountinfoService) { }

  ngOnInit() {
    this.getallcheckingaccounts();this.getallsavingsaccounts();
  }

  username = this.CookieFactory.get("username");
  firstName = this.CookieFactory.get("firstName");
  lastName = this.CookieFactory.get("lastName");
  loggedIn = this.CookieFactory.get("loggedIn");
  ID = this.CookieFactory.get("ID");


  public getallcheckingaccounts():void {
    this.checkingaccount.getCheckingaccountinfo().subscribe(
      (response: checkingaccountinfo[]) => {
        this.checkingaccountinfo = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  public getallsavingsaccounts():void {
    this.savingsaccount.getsavingsaccountinfo().subscribe(
      (response: savingsaccountinfo[]) => {
        this.savingsaccountinfo = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }


}
