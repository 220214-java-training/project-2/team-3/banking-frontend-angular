import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {constructor(private CookieFactory:CookieService){}
  title = 'banking';
  
  
  username = this.CookieFactory.get("username");
  firstName = this.CookieFactory.get("firstName");
  lastName = this.CookieFactory.get("lastName");
  loggedIn = this.CookieFactory.get("loggedIn");
  ID = this.CookieFactory.get("ID");



  logout() {
    this.CookieFactory.deleteAll();
    window.location.reload();

  }
  
}
