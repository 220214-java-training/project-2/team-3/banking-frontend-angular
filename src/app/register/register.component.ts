import { Component, OnInit } from '@angular/core';
import { Profile } from './profileModel';
import { ProfileService } from '../profile.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {        constructor(private http:HttpClient, private CookieFactory:CookieService) { }
  ngOnInit(): void {
    this.CookieFactory.delete('verified');
    
  }

  username = this.CookieFactory.get("username");
  firstName = this.CookieFactory.get("firstName");
  lastName = this.CookieFactory.get("lastName");
  loggedIn = this.CookieFactory.get("loggedIn");
  ID = this.CookieFactory.get("ID");
  
  destination = 'http://localhost:8888/registerNewAccount';
  profileDetails = new Profile();

    onSubmit(){
      
            let parameters = new HttpParams()
            .set('fname',this.profileDetails.fname)
            .set('midInitial',this.profileDetails.midInitial)
            .set('lname',this.profileDetails.lname)
            .set('email',this.profileDetails.email)
            .set('phone',this.profileDetails.phone)
            .set('street',this.profileDetails.street)
            .set('city',this.profileDetails.city)
            .set('state',this.profileDetails.state)
            .set('zipcode',this.profileDetails.zipcode)
            .set('ssNum',this.profileDetails.ssNum)
            .set('uname',this.profileDetails.uname)
            .set('pass',this.profileDetails.pass)
            .set('passConfirm',this.profileDetails.passConfirm);

            this.http.post(this.destination,parameters).subscribe();
        }
            

        

    }


